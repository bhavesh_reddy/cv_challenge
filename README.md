# CV Challenge

##About:
This is an early attempt ti extract
   * Arm swing direction analysis
   * To Calculate the number of trucks filled
   
   using a video from a ultra wide angle camera mounted on the cab of an excavator.

## Approach for Objective I:
Since the camera is fixed to the arm of the excavator motion along the excavator arm always stays in the center of the
 frame(change along x is always very minuscule and unreliable). This renders traditional feature point based trackers 
 unusable.
* For the motion in x- direction: 
    1. The avg optical flow of the frame is calculated and threshold is applied to calculate the 
    direction of swing.
    2. Can estimate the range(Quantity) if the calibration is known

* For Y- direction and Z- direction:
    1. A Neural network is trained to track the bucket on the arm of the excavator and the mid point of the 
    bounding box of the bucket is used for estimating the Y direction and the area is used to estimate the Z 
    direction.
    2. A Tiny-Yolov3 based model is trained using 368 images(not the Data-set provided, self annotated) for
     detection(Classes: Bucket, Truck) 
    3. The estimate is only as reliable as the model's detection.
* To improve, more data can be used to improve the detection of the excavator arm and rectification of the frame to 
remove distortion due to the omni-directional camera can be done.
## Approach for Objective II:
A traditional method with rules to define a trick being loaded can be cumbersome and will introduce high bias to the 
current scenario.

Hence a deep learning based classification method has been used to classify a frame into 'Truck being Loaded' or 'not loaded'
* The model has a Inception_v3 base and is fine tuned with 320 images of truck being 'loaded' and 48 images of 'not loaded'.
* The model has very low accuracy doe to the small training data (even after augmentation).
* It predicts 'Loaded' most of the frames due to skew in training data for both classes.
* The accuracy should improve with more data.


##Setup
Clone the repository

```
git clone https://bhavesh_reddy@bitbucket.org/bhavesh_reddy/cv_challenge.git
```
Install the required packages
```
pip3 install -r Requirements.txt
```
Download the models and videos from  https://drive.google.com/open?id=1jRmfpLGwwbphRCHdJ-KGMLn1VXYTFtQ2
##To Run the Program
```
python3 main_code.py -i <input_filename> -o <output_filename> \
                     -m <path/to/models/and/config/files> \
                     -c <confidence_threshold> -t <NMS threshold>
```
###Example
```
python3 -i videos/sample-3.mp4 -o videos/output.avi -m models
```
##Improvements
- [ ] Obtain more data
- [ ] Try using other detectors
- [ ] Try other Frameworks like MobileNet,Inception etc for object detection
- [ ] Try other classification frameworks.
- [ ] Remove Distortion
- [ ] Include the Right part of the video for excavator movement extraction.
