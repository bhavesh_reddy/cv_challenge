# import the necessary packages
import numpy as np
import argparse
import imutils
import time
import cv2
import os


# =================defim=ning exponential moving average function =========== #
def moving_average(inp_prev, inp, n = 20):
    alpha = 2./(1+n)
    EMA = inp*alpha + (1-alpha)*inp_prev

    return EMA


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", default='videos/sample-3.mp4',
                help="path to input image")
ap.add_argument("-o", "--output", default='videos/output.avi',
                help="path to output video")
ap.add_argument("-m", "--model", default='models',
                help="base path to models directory")
ap.add_argument("-c", "--confidence", type=float, default=0.5,
                help="minimum probability to filter weak detections")
ap.add_argument("-t", "--threshold", type=float, default=0.3,
                help="threshold when applying non-maximal suppression")
args = vars(ap.parse_args())

# ===========initializing the classification model{loading/not loading of truck} based on Inception_v3============= #

graph_path_classification = os.path.sep.join([args["model"], "inference_output_graph.pb"])
print("[INFO] : loading classification model from disk...")
LoadingNet = cv2.dnn.readNetFromTensorflow(graph_path_classification)
labels_classPath = os.path.sep.join([args["model"], "classes.names"])
LABELS_class = open(labels_classPath).read().strip().split("\n")

# ============initializing the tracking model{bucket and Truck track} based on tiny_yolov3======================= #
labels_trackPath = os.path.sep.join([args["model"], "objects.names"])
LABELS_track = open(labels_trackPath).read().strip().split("\n")
weightsPath_track= os.path.sep.join([args["model"], "yolov3-tiny_50000.weights"])
configPath_track = os.path.sep.join([args["model"], "yolov3-tiny.cfg"])
print("[INFO] : loading tracking from disk...")
TrackNet = cv2.dnn.readNetFromDarknet(configPath_track, weightsPath_track)
ln = TrackNet.getLayerNames()
ln = [ln[i[0] - 1] for i in TrackNet.getUnconnectedOutLayers()]

# =======================initialize a list of colors================================================ #
np.random.seed(42)
COLORS = np.random.randint(0, 255, size=(10, 3), dtype="uint8")

# ====================================Initialize the video stream and its parameters========================== #
vs = cv2.VideoCapture(args["input"])
writer = None
W = int(vs.get(cv2.CAP_PROP_FRAME_WIDTH))
H = int(vs.get(cv2.CAP_PROP_FRAME_HEIGHT))
_, prev_frame = vs.read()
prev_frame = prev_frame[:, :int(W/2)]
prev_frame = cv2.resize(prev_frame, (1024, 768))
prev_grey = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2GRAY)

# determine number of frame in the video
try:
    prop = cv2.CV_CAP_PROP_FRAME_COUNT if imutils.is_cv2() \
        else cv2.CAP_PROP_FRAME_COUNT
    total = int(vs.get(prop))
    print("[INFO] : {} total frames in video".format(total))

# an error occurred while trying to determine the total
# number of frames in the video file
except:
    print("[INFO] : could not determine # of frames in video")
    print("[INFO] : no approx. completion time can be provided")
    total = -1

frameId = 0
(x_c_prev, y_c_prev, area_prev) = (0, 0, 0)
num_up_down = 0
num_towards = 0
num_turns = 0
# ==========================starting Loop======================= #
while True:
    not_noving_side = 0
    not_moving_away = 0
    not_moving_up = 0
    # extract feature points from init frame
    prev_pts = cv2.goodFeaturesToTrack(prev_grey,
                                       maxCorners=200,
                                       qualityLevel=0.01,
                                       minDistance=30,
                                       blockSize=3)
    success, frame = vs.read()

    # stop loop
    if not success:
        print("[INFO] : error extracting frame from video")
        break
    # using the only left part of the video
    frame = frame[:, :int(W/2)]
    curr = cv2.resize(frame, (1024, 768))
    curr_grey = cv2.cvtColor(curr, cv2.COLOR_BGR2GRAY)
    # construct a blob from the input frame to then perform a forward pass
    blob_class = cv2.dnn.blobFromImage(frame, size=(416, 416), swapRB=True, crop=False)
    blob_track = cv2.dnn.blobFromImage(frame, 1 / 255.0, (416, 416), swapRB=True, crop=False)
    LoadingNet.setInput(blob_class)
    TrackNet.setInput(blob_track)

    start = time.time()
    classOutput = LoadingNet.forward()
    trackOutputs = TrackNet.forward(ln)
    end = time.time()
    # initialize our lists of detected bounding boxes, confidences,
    # and class IDs, respectively
    boxes = []
    confidences_track = []
    classIDs_track = []
    # loop over each of the layer outputs
    for output in trackOutputs:
        # loop over each of the detections
        for detection in output:
            # extract the class ID and confidence (i.e., probability)
            # of the current object detection
            scores = detection[5:]
            classID = np.argmax(scores)
            confidence = scores[classID]

            # filter out weak predictions by ensuring the detected
            # probability is greater than the minimum probability
            if confidence > args["confidence"]:
                # scale the bounding box coordinates back relative to
                # the size of the image, keeping in mind that YOLO
                # actually returns the center (x, y)-coordinates of
                # the bounding box followed by the boxes' width and
                # height
                box = detection[0:4] * np.array([W/2, H, W/2, H])
                (centerX, centerY, width, height) = box.astype("int")

                # use the center (x, y)-coordinates to derive the top
                # and and left corner of the bounding box
                x = int(centerX - (width / 2))
                y = int(centerY - (height / 2))

                # update our list of bounding box coordinates,
                # confidences, and class IDs
                boxes.append([x, y, int(width), int(height)])
                confidences_track.append(float(confidence))
                classIDs_track.append(classID)
    # apply non-maxima suppression to suppress weak, overlapping
    # bounding boxes
    idxs = cv2.dnn.NMSBoxes(boxes, confidences_track, args["confidence"],
                            args["threshold"])
    if len(idxs) > 0:
        # loop over the indexes we are keeping
        for i in idxs.flatten():
            if classIDs_track[i] != 0:
                continue
            # extract the bounding box coordinates
            (x, y) = (boxes[i][0], boxes[i][1])
            (w, h) = (boxes[i][2], boxes[i][3])
            (x_c, y_c) = (int(x+(w/2)), int(y+(h/2)))
            # dx = x_c_prev - x_c
            dy = y_c_prev - y_c
            d_area = area_prev - (w*h)
            if dy > 5:
                text_up_down = 'up'
            elif dy < -5:
                text_up_down = 'down'
            else:
                text_up_down = 'no y direction movement'
                not_moving_up = 1
            # TODO: Work on improving threshold
            if d_area > 500:
                text_away = 'away'
            elif d_area < -500:
                text_away = 'towards'
            else:
                text_away = 'no z direction movement'
                not_moving_away = 1
            (x_c_prev, y_c_prev, area_prev) = (x_c, y_c, w*h)

            # draw a bounding box rectangle and label on the frame
            color = [int(c) for c in COLORS[classIDs_track[i]]]
            cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
            text = "{}: {:.4f}".format(LABELS_track[classIDs_track[i]],
                                       confidences_track[i])
            cv2.putText(frame, text, (x, y - 5),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    else:
        text_up_down = 'lost'
        text_away = 'lost'
    if max(classOutput[0]) > 0.6:
        id = np.where(classOutput[0] == max(classOutput[0]))
        if np.squeeze(id) == 0:
            text_class = 'Loading'
        else:
            text_class = 'Not Loading'
    else:
        text_class = 'unknown'
    curr_pts, status, err = cv2.calcOpticalFlowPyrLK(prev_grey, curr_grey, prev_pts, None)

    assert prev_pts.shape == curr_pts.shape

    # Filter only valid points
    idd = np.where(status == 1)[0]
    prev_pts = prev_pts[idd]
    curr_pts = curr_pts[idd]

    # Find transformation matrix
    m = cv2.estimateRigidTransform(prev_pts, curr_pts, fullAffine=False)  # will only work with OpenCV-3 or less
    # Extract translation
    dx = m[0, 2]
    # dy = m[1, 2]

    if dx > 2:
        text_sideways = 'Left'
    elif dx < -2:
        text_sideways = 'right'
    else:
        text_sideways = 'no x direction movement'
        not_noving_side = 1

    # if not_moving_away and not_moving_up and not_noving_side:
    #     cv2.putText(frame, text_class, (15, 100),
    #                 cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255, 0, 255), 2)
    #     text_not = 'no movement'
    #     cv2.putText(frame, text_not, (15, 150),
    #                 cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255, 0, 255), 2)

    cv2.putText(frame, text_class, (15, 100),
                cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255, 0, 255), 2)
    cv2.putText(frame, text_up_down, (15, 150),
                cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255, 0, 255), 2)
    cv2.putText(frame, text_sideways, (15, 200),
                cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255, 0, 255), 2)
    cv2.putText(frame, text_away, (15, 250),
                cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255, 0, 255), 2)
    # check if the video writer is None
    if writer is None:
        # initialize our video writer
        fourcc = cv2.VideoWriter_fourcc(*"MJPG")
        writer = cv2.VideoWriter(args["output"], fourcc, 30,
                                 (frame.shape[1], frame.shape[0]), True)

        # some information on processing single frame
        if total > 0:
            elap = (end - start)
            print("[INFO] single frame took {:.4f} seconds".format(elap))
            print("[INFO] estimated total time to finish: {:.4f}".format(
                elap * total))

    # write the output frame to disk
    writer.write(frame)
    prev_grey = curr_grey
    frame = cv2.resize(frame, (1024, 768))
    cv2.imshow('output', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# release the file pointers
print("[INFO] cleaning up...")
# writer.release()
vs.release()



